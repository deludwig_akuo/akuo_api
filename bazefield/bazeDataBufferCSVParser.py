import datetime

class Calculator:

    def ProcessData(self, rawTime, rawValues, retTime, retAvg, startTime, endTime):
        try:
            window = 20     # Seconds
            maximum = 0
            total = 0
            # average = 0
            i = 0
            rowCounter = 0

            intervalStart = 0
            # thisRowTime = 0
            # lastRowTime = 0
            lastTicker = 0
            # xTickers = []
            xLabels = []


            # Check to make sure the value is not null
            for value in rawValues:
                if value != '':
                    if rawTime[rowCounter] >= startTime:
                        if rawTime[rowCounter] <= endTime:
                            thisRowTime = rawTime[rowCounter]
                            if intervalStart == 0:
                                intervalStart = thisRowTime

                            # do calculations for this window
                            lastRowTime = thisRowTime
                            i = i + 1

                            # Average
                            total = total + float(value)
                            average = total / i

                            # Max
                            if maximum < float(value):
                                maximum = float(value)

                            # Check if it has been 20 minutes
                            deltaTime = thisRowTime - intervalStart
                            deltaSeconds = deltaTime.total_seconds()

                            if deltaSeconds > window:
                                # Time to start a new calculation window
                                retTime.append(lastRowTime)
                                retAvg.append(average)
                                # retMax.append(maximum)
                                # retBenchmark.append(9.6)
                                intervalStart = 0
                                total = 0
                                average = 0
                                i = 0
                                maximum = 0

                            # Calculate x axis ticks
                            if lastTicker == 0:
                                lastTicker = thisRowTime
                            else:
                                xtime = thisRowTime - lastTicker
                                xSeconds = xtime.total_seconds()
                                if xSeconds >= 3600 * 12:
                                    # xTicks.append(thisRowTime)
                                    lastTicker = thisRowTime
                                    xLabels.append(str(thisRowTime))

                rowCounter = rowCounter + 1


        except Exception as ex:
            print("Error in MetCalculator.ProcessData")