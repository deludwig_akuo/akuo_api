


class BazeTagMaker:

    def __init__(self):
        try:
            x = 7

        except Exception as ex:
            print('Error in init: ' + str(ex))


    def makeGeneralTurbineTags(self, tags, prefix, numTurbines, postfix):
        try:
            counter = 1

            while counter <= numTurbines:
                turbineId = ''
                if counter < 10:
                    turbineId = '00' + str(counter)
                elif counter > 9:
                    turbineId = '0' + str(counter)

                tags.append(prefix + turbineId + postfix)
                counter = counter + 1

        except Exception as ex:
            print('Error in makeGeneralTurbineTags: ' + str(ex))


    def makeActivePowerTagBennAllTurbines(self, tags):
        try:
            NUMBER_OF_TURBINES = 31
            counter = 1
            'ESCA-SUB001-TO_AKUO_MODBUS.IREG_00249_Mast_1_Wind_Speed_82_meters'

            while counter <= NUMBER_OF_TURBINES:
                turbineId = ''
                if counter < 10:
                    turbineId = '00'+ str(counter)
                elif counter > 9:
                    turbineId = '0' + str(counter)
                tags.append('BENN-WTG' + turbineId + '-ActivePower')
                counter = counter + 1

        except Exception as ex:
            print('Error in makeActivePowerTagGEAllTurbines: ' + str(ex))


    def makeBenningtonTurbinesPoint(self, tags, pointName, numberOfTags, tagList):
        try:
            NUMBER_OF_TURBINES = numberOfTags
            counter = 1

            while counter <= NUMBER_OF_TURBINES:
                turbineId = ''
                if counter < 10:
                    turbineId = '00' + str(counter)
                elif counter > 9:
                    turbineId = '0' + str(counter)
                tags.append('BENN-WTG' + turbineId + '-' + pointName)
                tagList.append('BENN-WTG' + turbineId + '-' + pointName)
                counter = counter + 1

        except Exception as ex:
            print('Error in make TurbineWindSpeed: ' + str(ex))