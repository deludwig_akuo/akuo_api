import time
import datetime
from bazefield.base import Base
import requests
import json
from enum import Enum
from datetime import  datetime
import sys, os
from datetime import timezone
from datetime import timedelta
# from logger import MyLogger


class APIBaze:

    logger = None

    class aggr(Enum):
        INTERPOLATIVE = 1,
        TOTAL = 2,
        AVERAGE = 3,
        TIMEAVERAGE = 4,
        COUNT = 5,
        STDEV = 6,
        MINIMUMACTUALTIME = 7,
        MINIMUM = 8,
        MAXIMUMACTUALTIME = 9,
        MAXIMUM = 10,
        START = 11,
        END = 12,
        DELTA = 13,
        RANGE = 18,
        DURATIONGOOD = 19,
        DURATIONBAD = 20,
        PERCENTGOOD = 21,
        PERCENTBAD = 22,
        WORSTQUALITY = 23,
        INTRPOLATIVE_START = 100,
        INTERPOLATIVE_END = 101,
        SUM = 102,

    # def __init__(self):


    def getRealTimeValue(self, tag):
        urlMeasurements = 'measurements/'
        base = Base()

        session = requests.Session()
        session.headers.update(base.headers)
        ret = session.get(base.baseURL + urlMeasurements + tag)

        data = json.loads(ret.text)
        retValue = data['measurements'][0].get('value').get('v')

        return retValue


    # def getHistData(self, tag):
    def getHistDataSingleTag(self, tag, aggregate, startDate, endDate, intervalSec):
        try:
            base = Base()
            url = 'measurements/' + tag + '/aggregates/' + aggregate + '/from/' + str(startDate) + '/to/' + str(endDate) + '/interval/' + str(intervalSec) + '?format=json'

            session = requests.Session()
            session.headers.update(base.headers)
            ret = session.get(base.baseURL + url)

            jsonData = json.loads(ret.text)
            timeSeriesList = jsonData.get('timeSeriesList', '')
            if len(timeSeriesList) == 0:
                timeSeries = None
            else:
                timeSeries = timeSeriesList[0]['timeSeries']

            return timeSeries
        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error(exc_type  + '---' +  fname  + '---' +  exc_tb.tb_lineno)


    def makeDateTime(self, month, day):
        try:
            dtCST = datetime(year=2022,
                            month=month,
                            day=day,
                            hour=0,
                            minute=0,
                            second=0)

            dtCST = dtCST.replace(tzinfo=timezone.utc) + timedelta(hours=6)
            retVal = int(dtCST.timestamp()) * 1000
            return retVal

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error(exc_type + '---' + fname + '---' + exc_tb.tb_lineno)


    def makeDateTimeWithYear(self, month, day, year):
        try:
            dtCST = datetime(year=year,
                            month=month,
                            day=day,
                            hour=0,
                            minute=0,
                            second=0)

            dtCST = dtCST.replace(tzinfo=timezone.utc) + timedelta(hours=6)
            retVal = int(dtCST.timestamp()) * 1000
            return retVal

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            self.logger.error(exc_type + '---' + fname + '---' + exc_tb.tb_lineno)