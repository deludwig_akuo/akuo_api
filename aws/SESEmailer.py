import boto3



class SESEmailer:
    SENDER = "Sender Name <notifications@akuousa.com>"
    AWS_REGION = "us-east-1"
    CHARSET = "UTF-8"


    # def sendaccountRequestEmails(self, newUserEmail, newUserUsername)



    def sendEmail(self, RECIPIENT, SUBJECT, BODY_TEXT, BODY_HTML):
        try:
            client = boto3.client('ses', region_name=self.AWS_REGION)

            # Provide the contents of the email.
            response = client.send_email(
                Destination={
                    'ToAddresses': [
                        RECIPIENT,
                    ],
                },
                Message={
                    'Body': {
                        # 'Html': {
                        #     'Charset': self.CHARSET,
                        #     'Data': BODY_HTML,
                        # },
                        'Text': {
                            'Charset': self.CHARSET,
                            'Data': BODY_TEXT,
                        },
                    },
                    'Subject': {
                        'Charset': self.CHARSET,
                        'Data': SUBJECT,
                    },
                },
                Source=self.SENDER,
                # If you are not using a configuration set, comment or delete the
                # following line
                # ConfigurationSetName=CONFIGURATION_SET,
            )

            print(response)

        except Exception as ex:
            print('Error in SESEmailer.sendEmail: ' + str(ex))
