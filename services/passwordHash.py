from passlib.hash import pbkdf2_sha256 as sha256

class PasswordHash:

    def generateHash(self, password):
        try:
            return sha256.hash(password)
        except Exception as ex:
            print('Error in generateHash: ' + str(ex))

    def verifyHash(self, password, hash):
        try:
            return sha256.verify(password, hash)
        except Exception as ex:
            print('Error in verifyHash: ' + str(ex))