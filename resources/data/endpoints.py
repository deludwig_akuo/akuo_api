from resources.data.benn import Benn


def add_endpoints(api):
    print("Adding deliverable endpoints...")
    api.add_resource(Benn, '/data/benn')
