from flask_restful import Resource
import os, sys
from bazefield.api_baze import APIBaze
from flask_jwt_extended import (jwt_required)

class Benn(Resource):
    @jwt_required()
    def get(self):
        try:

            # Get data from API
            baze = APIBaze()

            apTag = 'BENN-OCM001-ActivePowerW'
            wsTag = 'BENN-MET001-WindSpeed'
            # tempTag = 'BENN-MET001-AmbientTemp'
            tempTag = 'BENN-WTG001-AmbientTemp'

            apData = baze.getRealTimeValue(apTag)
            wsData = baze.getRealTimeValue(wsTag)
            tempData = baze.getRealTimeValue(tempTag)

            retVal = {'activepower': apData, 'windspeed': wsData, 'temperature': tempData}

            # Return the data
            print('User collected Bennington data.')
            return retVal

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)