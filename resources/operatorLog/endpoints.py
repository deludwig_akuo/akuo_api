from resources.operatorLog.addOperatorLog import AddOperatorLog
from resources.operatorLog.getOperatorLogs import GetOperatorLog
from resources.operatorLog.getProjects import GetProjectsSimple

def add_endpoints(api):
    print("Adding deliverable endpoints...")
    api.add_resource(AddOperatorLog, '/operatorlog/addoperatorlog')
    api.add_resource(GetOperatorLog, '/operatorlog/getoperatorlogs')
    api.add_resource(GetProjectsSimple, '/operatorlog/getprojects')
    # api.add_resource(GetCompletedDeliveries, '/com/getcompleteddeliverables')