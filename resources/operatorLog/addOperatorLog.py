from flask_restful import Resource
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import obligations
from database.models import OperatorLog
from database.models import deliverables
from database.db import Session
import datetime


class AddOperatorLog(Resource):

    def post(self):
        try:
            timestamp = request.json.get('timestamp', datetime.datetime.now())
            invoiceable = request.json.get('invoiceable', False)
            projectId = request.json.get('projectId', '0')
            projectName = request.json.get('projectId', 'unknown')
            equipmentName = request.json.get('equipmentname', '')
            type = request.json.get('type', '')
            mal = request.json.get('mal', False)
            breakerClosed = request.json.get('breakerClosed', False)
            breakerName = request.json.get('breakerClosed', 'NA')
            notes = request.json.get('notes', '')
            description = request.json.get('description', '')

            retVal = []

            # Now go get all the records again and return them
            dbData = Session.query(OperatorLog).all()
            for prj in dbData:
                for ob in prj.obligations:
                    for de in ob.deliverables:
                        if de.isComplete:
                            due = str(de.dueDate)
                            due = due[:-12]
                            retVal.append({'id': de.id,
                                           'projectId': de.projectId,
                                           'obligationId': de.obligationId,
                                           'dueDate': due,
                                           'isComplete': de.isComplete,
                                           'hoursToComplete': de.hoursToComplete,
                                           'projectName': prj.name,
                                           'description': ob.description,
                                           'recipients': ob.recipients,
                                           'contractName': ob.contractName})

            Session.remove()
            return retVal

        except Exception as ex:
            print('Error in GetProjects')
