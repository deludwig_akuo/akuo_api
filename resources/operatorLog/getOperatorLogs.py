from flask_restful import Resource
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import obligations
from database.models import OperatorLog
from database.models import deliverables
from database.db import Session
import datetime


class GetOperatorLog(Resource):

    def get(self):
        try:
            retVal = []

            # Now go get all the records again and return them
            dbData = Session.query(OperatorLog).all()
            for operation in dbData:
                retVal.append({'id': operation.id,
                               'breakerName': operation.breakerName,
                               'closedBreaker': operation.closedBreaker,
                               'description': operation.description,
                               'equipmentName': operation.equipmentName,
                               'invoiceable': operation.invoiceable,
                               'mal': operation.mal,
                               'notes': operation.notes,
                               'projectId': operation.projectId,
                               'projectName': operation.project.name,
                               'tagId': operation.tagId,
                               'tagName': operation.tag.name,
                               'userId': operation.userId,
                               'userName': operation.user.username,
                               'timestamp': str(operation.timestamp),
                               'type': operation.type
                               })

            Session.remove()
            return retVal

        except Exception as ex:
            print('Error in GetProjects')
