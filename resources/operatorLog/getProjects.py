from flask_restful import Resource
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import obligations
from database.models import OperatorLog
from database.models import projects
from database.db import Session
import datetime


class GetProjectsSimple(Resource):

    def get(self):
        try:
            retVal = []

            # Now go get all the records again and return them
            dbData = Session.query(projects).all()
            for project in dbData:
                retVal.append({'projectId': project.id,
                               'projectName': project.name,
                               })

            Session.remove()
            return retVal

        except Exception as ex:
            print('Error in GetProjects')
