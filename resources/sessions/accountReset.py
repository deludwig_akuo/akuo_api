import string
from aws.SESEmailer import SESEmailer
from flask_restful import Resource
from flask import request, jsonify
from random import *
from database.models import Users
from database.db import Session
from services.passwordHash import PasswordHash


class AccountReset(Resource):

    def post(self):
        try:
            email = request.json.get('email', None)
            if email is None:
                return jsonify({'Message': 'Error, need email address', 'status': 0})

            allchar = string.ascii_letters + string.digits
            resetKey = "".join(choice(allchar) for x in range(50))

            # Get the user from the db
            user = Session.query(Users).filter(Users.email == email.lower()).first()
            if user is None:
                return jsonify({'Message': 'Error, email address not found', 'status': 0})

            user.accountResetKey = resetKey
            user.commit()

            # Send email to user that requested access
            subject = 'Akuo password reset'
            body = 'This is your Akuo portal password reset email. Please go to https://api.akuo.com/sessions/reset/' + resetKey + ' to complete the reset'

            emailSender = SESEmailer()
            emailSender.sendEmail(email, subject, body, "")

            return jsonify({'Message': 'Reset email sent', 'status': 1})

        except Exception as ex:
            print('Error in resetPassword')


    def put(self):
        try:
            key = request.json.get('key', None)
            password = request.json.get('password', None)

            if key is None or password is None:
                return jsonify({'Message': 'Reset key or new password is missing', 'status': 0})

            # Hash the password then save to the user
            hasher = PasswordHash()
            password = hasher.generateHash(password)

            user = Session.query(Users).filter(Users.accountResetKey == key).first()
            if user is None:
                return jsonify({'Message': 'Error, the reset key cannot be found', 'status': 0})
            user.password = password
            Session.commit()
            return jsonify({'Message': 'Password has been successfully reset', 'status': 1})

        except Exception as ex:
            print('Error in resetPassword')