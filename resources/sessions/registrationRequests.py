from flask_restful import Resource
from flask import request, jsonify
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import Users
from database.db import Session
import re
from services.passwordHash import PasswordHash
from aws.SESEmailer import SESEmailer
import sys, os
from passlib.hash import pbkdf2_sha256 as sha256


class RegistrationRequests(Resource):

    def post(self):
        try:
            username = request.json.get('username', None)
            username = username.lower()
            password = request.json.get('password', None)
            email = request.json.get('email', None)
            email = email.lower()
            # Check to see if the user already exists
            userList = Session.query(Users).all()
            for user in userList:
                if user.email.lower() == email.lower() or user.username.lower() == username.lower():
                    return jsonify({'Message': 'Error, username or password already exist', 'success': 0})

            # Check to make sure the email is in email format
            # if not self.checkEmail(email):
            #     return jsonify({'Message': 'Error, email is not in the correct format', 'success': 0})

            # Encrypt the password
            hasher = PasswordHash()
            password = sha256.hash(password)

            # Add the account
            newUser = Users()
            newUser.username = username
            newUser.email = email
            # newUser.phone = phone
            newUser.password = password
            Session.add(newUser)

            # userList = Session.query(Users).all()
            # userList.append(newUser)
            Session.commit()

            # Send email to user that requested access
            subject = 'Akuo ASM account is under review'
            body = 'username,' \
                   'Your account Request is being reviewed. I will let you know once it is approved.'

            emailSender = SESEmailer()
            emailSender.sendEmail(email, subject, body, '')

            # Send email to myself so that I can approve the user
            emailSender.sendEmail('dludwig@tutanota.com', 'User requested access to Akuo ASM', 'Approve the user', "")


            return jsonify({'Message': 'User request sucessfully added', 'success': 1})

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


    def checkEmail(self, email):
        regex = r'\b[A-Za-z0-9._%+_] +@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if re.fullmatch(regex, email):
            return True
        else:
            return False
