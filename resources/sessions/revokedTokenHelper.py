from database.db import Session
from database.models import RevokedTokens
import sys, os


class RevokedTokenHelper:

    def is_jti_blacklisted(cls, jti):
        try:
            revokedToken = Session.query(RevokedTokens).filter(RevokedTokens.jti == jti).first()
            if revokedToken is None:
                return False
            else:
                return True

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
