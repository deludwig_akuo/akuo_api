from flask_restful import Resource
from flask import request, jsonify
from passlib.hash import pbkdf2_sha256 as sha256
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import Users
from flask_jwt_extended import (create_access_token)
from database.models import projects
from database.models import deliverables
from database.db import Session
from sqlalchemy import or_
from passlib.hash import pbkdf2_sha256 as sha256

import datetime


class Login(Resource):

    def post(self):
        try:
            username = request.json.get('username', None)
            password = request.json.get('password', None)

            if not request.is_json:
                return {"message": "Missing JSON in request"}, 200

            if username == "" or username is None:
                return {'Message': 'Error, username cannot be blank', 'success': 0}

            if password == "" or password is None:
                return {'Message': 'Error, password cannot be blank', 'success': 0}

            # Get the users hashed password
            userInDb = Session.query(Users).filter(or_(Users.username.like('%' + username.lower() + '%'), Users.email.like('%' + username.lower() + '%'))).first()
            if userInDb is None:
                return {'message': 'Error, username not found', 'success': 0}

            # Compare the password hashes and see if they are the same
            if sha256.verify(password, userInDb.password):
                expires = datetime.timedelta(days=180)
                access_token = create_access_token(identity=username, expires_delta=expires)
                print('User successfully logged in.')
                return {'access_token': access_token}, 200

            print('Failed login attempt.')
            return {'Message': 'access denied', 'status': 0}

        except Exception as ex:
            print('Error in GetProjects')



    def validate_email(self, email):
        if re.match("\A(?P<name>[\w\-_]+)@(?P<domain>[\w\-_]+).(?P<toplevel>[\w]+)\Z", email, re.IGNORECASE):
            return True
        else:
            return False


