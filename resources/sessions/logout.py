from flask_restful import Resource
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import RevokedTokens
from database.models import projects
from database.models import deliverables
from database.db import Session
import datetime
from flask_jwt_extended import (jwt_required, get_jwt)


class Logout(Resource):
    @jwt_required()
    def post(self):
        try:
            jti = get_jwt()['jti']

            revokedTokenList = Session.query(RevokedTokens).filter(RevokedTokens.jti == jti).all()

            newRevokedToken = RevokedTokens()
            newRevokedToken.jti = jti
            # revokedTokenList.append(newRevokedToken)
            Session.add(newRevokedToken)
            Session.commit()
            print('User Logged out.')
            return {'Message': 'logged out', 'status': 1}

        except Exception as ex:
            print('Error in GetProjects')
