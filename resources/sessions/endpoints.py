from resources.sessions.accountReset import AccountReset
from resources.sessions.approveRegistrationRequest import ApproveRegistrationRequests
from resources.sessions.changePassword import ChangePassword
from resources.sessions.login import Login
from resources.sessions.logout import Logout
from resources.sessions.registrationRequests import RegistrationRequests


def add_endpoints(api):
    try:
        print("Adding sessions endpoints...")
        api.add_resource(AccountReset, '/sessions/resetaccount')
        api.add_resource(ApproveRegistrationRequests, '/sessions/approveregistrationrequest')
        api.add_resource(ChangePassword, '/sessions/changepassword')
        api.add_resource(Login, '/sessions/login')
        api.add_resource(Logout, '/sessions/logout')
        api.add_resource(RegistrationRequests, '/sessions/register')

    except Exception as ex:
        print('Error in sessions_endpoints: ' + str(ex))