from flask_restful import Resource
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import obligations
from database.models import projects
from database.models import deliverables
from database.db import Session
import datetime
from resources.com.deliverables.deliverablesService import DeliverablesService


class UpdateDeliverables(Resource):

    def post(self):
        try:
            id = request.json.get('id', 0)
            isComplete = request.json.get('isComplete', False)
            hoursToComplete = int(request.json.get('hoursToComplete', 0))

            deliverableToEdit = Session.query(deliverables).filter(deliverables.id == id).first()
            if deliverableToEdit is None:
                return 'Could not find the deliverable'

            deliverableToEdit.isComplete = isComplete
            deliverableToEdit.hoursToComplete = hoursToComplete
            Session.commit()

            # Now return all the deliverables
            # Now go get all the records again and return them
            service = DeliverablesService()
            return service.getOpenDeliverables()

            # retVal = []
            # dbData = Session.query(projects).all()
            # for prj in dbData:
            #     for ob in prj.obligations:
            #         for de in ob.deliverables:
            #             if not de.isComplete:
            #                 due = str(de.dueDate)
            #                 due = due[:-12]
            #                 retVal.append({'id': de.id,
            #                                'projectId': de.projectId,
            #                                'obligationId': de.obligationId,
            #                                'dueDate': due,
            #                                'isComplete': de.isComplete,
            #                                'hoursToComplete': de.hoursToComplete,
            #                                'projectName': prj.name,
            #                                'description': ob.description,
            #                                'recipients': ob.recipients,
            #                                'contractName': ob.contractName})
            #
            # Session.remove()
            # return retVal

        except Exception as ex:
            print('Error in UpdateDeliverables.post: ' + str(ex))
