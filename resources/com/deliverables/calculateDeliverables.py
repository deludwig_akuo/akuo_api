from flask_restful import Resource
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
import database.models
from database.models import obligations
from database.models import *
from database.models import deliverables
from database.db import Session
import datetime
import sys, os
from resources.com.deliverables.deliverablesService import DeliverablesService


class CalculateDeliverables(Resource):

    def get(self):
        try:
            retVal = []
            # TODO: Add rest of code
            # Get a list of all the obligations
            dbData = Session.query(projects).all()
            nowTime = datetime.datetime.now()
            # dbDeliverables = Session.query(projects).all()


            toAddArray = []
            for prj in dbData:
                for ob in prj.obligations:
                    # Do one year in advance
                    # Determine the time from now for the next item to be added
                    if ob.enabled:
                        # Calculate deliverables for next 12 months
                        if ob.frequency == 'Monthly':
                            # Add items for the next 3 months from this month

                            loopMonth = nowTime.month
                            while loopMonth < 13:
                                if len(ob.deliverables) < 1:
                                    newDeliverable = self.createDeliverable(ob, nowTime.year, loopMonth)
                                    ob.deliverables.append(newDeliverable)
                                else:
                                    found = False
                                    for deliverable in ob.deliverables:
                                        if (deliverable.dueDate.year == nowTime.year and deliverable.dueDate.month == loopMonth):
                                            found = True
                                            break

                                    if not found:
                                        newDeliverable = self.createDeliverable(ob, nowTime.year, loopMonth)
                                        ob.deliverables.append(newDeliverable)

                                loopMonth = loopMonth + 1

                        if ob.frequency == 'Quarterly':
                            # Calculate deliverable for each quarter. Add any missing records for the current year
                            counter = 1
                            while counter <= 11:
                                if len(ob.deliverables) < 1:
                                    newDeliverable = self.createDeliverable(ob, nowTime.year, counter)
                                    ob.deliverables.append(newDeliverable)
                                else:
                                    found = False
                                    for deliverable in ob.deliverables:
                                        if (deliverable.dueDate.year == nowTime.year and deliverable.dueDate.month == counter):
                                            found = True
                                            break

                                    if not found:
                                        newDeliverable = self.createDeliverable(ob, nowTime.year, counter)
                                        ob.deliverables.append(newDeliverable)

                                counter = counter + 3

                        if ob.frequency == 'Annually':
                            if len(ob.deliverables) < 1:
                                newDeliverable = self.createDeliverable(ob, nowTime.year, ob.startMonth)
                                ob.deliverables.append(newDeliverable)
                            else:
                                found = False
                                for deliverable in ob.deliverables:
                                    if (deliverable.dueDate.year == nowTime.year):
                                        found = True
                                        break

                                if not found:
                                    newDeliverable = self.createDeliverable(ob, nowTime.year, ob.startMonth)
                                    ob.deliverables.append(newDeliverable)

            # Now add all the new items to the database

            Session.commit()

            service = DeliverablesService()
            return service.getOpenDeliverables()

            # Now go get all the records again and return them
            # dbData = Session.query(projects).all()
            # for prj in dbData:
            #     for ob in prj.obligations:
            #         for de in ob.deliverables:
            #             if not de.isComplete:
            #                 due = str(de.dueDate)
            #                 due = due[:-12]
            #                 retVal.append({'id': de.id,
            #                                'projectId': de.projectId,
            #                                'obligationId': de.obligationId,
            #                                'dueDate': due,
            #                                'isComplete': de.isComplete,
            #                                'hoursToComplete': de.hoursToComplete,
            #                                'projectName': prj.name,
            #                                'description': ob.description,
            #                                'recipients': ob.recipients,
            #                                'contractName': ob.contractName,
            #                                'catagory': ob.catagory,
            #                                'responsible': ob.responsible,
            #                                'notes': ob.notes})
            #
            # Session.remove()
            # return retVal

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)


    def createDeliverable(self, obligation, year, month):
        # Account for delays that are longer than a month
        month = month + int(obligation.delay / 30)
        days = 1 + obligation.delay % 30

        # Save the record
        newDeliverable = database.models.deliverables()
        newDeliverable.dueDate = datetime.datetime(year, month, days, 0, 0, 0)
        newDeliverable.isComplete = False

        return newDeliverable


