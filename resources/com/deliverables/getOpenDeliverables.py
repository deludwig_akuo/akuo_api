from flask_restful import Resource
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import obligations
from database.models import projects
from database.models import deliverables
from database.db import Session
import datetime
from resources.com.deliverables.deliverablesService import DeliverablesService


class GetOpenDeliverables(Resource):

    def get(self):
        try:
            service = DeliverablesService()
            return service.getOpenDeliverables()
            # retVal = []
            #
            # # Now go get all the records again and return them
            # dbData = Session.query(projects).all()
            # for prj in dbData:
            #     for ob in prj.obligations:
            #         for de in ob.deliverables:
            #             if not de.isComplete:
            #                 due = str(de.dueDate)
            #                 due = due[:-12]
            #                 retVal.append({'id': de.id,
            #                                'projectId': de.projectId,
            #                                'obligationId': de.obligationId,
            #                                'due': due,
            #                                'isComplete': de.isComplete,
            #                                'hoursToComplete': de.hoursToComplete,
            #                                'project': prj.name,
            #                                'description': ob.description,
            #                                'recipients': ob.recipients,
            #                                'contract': ob.contractName,
            #                                'catagory': ob.catagory,
            #                                'responsible': ob.responsible,
            #                                'notes': ob.notes})
            #
            # Session.remove()
            # return retVal

        except Exception as ex:
            print('Error in GetProjects')