from database.models import projects
from database.models import deliverables
from database.db import Session

class DeliverablesService:

    def getOpenDeliverables(self):

        retVal = []
        dbData = Session.query(projects).all()
        for prj in dbData:
            for ob in prj.obligations:
                for de in ob.deliverables:
                    if not de.isComplete:
                        due = str(de.dueDate)
                        due = due[:-12]
                        retVal.append({'id': de.id,
                                       'projectId': de.projectId,
                                       'obligationId': de.obligationId,
                                       'due': due,
                                       'isComplete': de.isComplete,
                                       'hoursToComplete': de.hoursToComplete,
                                       'project': prj.name,
                                       'description': ob.description,
                                       'recipients': ob.recipients,
                                       'contract': ob.contractName,
                                       'catagory': ob.catagory,
                                       'responsible': ob.responsible,
                                       'notes': ob.notes})

        Session.remove()
        return retVal