from resources.com.deliverables.calculateDeliverables import CalculateDeliverables
from resources.com.deliverables.updateDeliverables import UpdateDeliverables
from resources.com.deliverables.getCompletedDeliverables import GetCompletedDeliveries
from resources.com.deliverables.getOpenDeliverables import GetOpenDeliverables

def add_endpoints(api):
    print("Adding deliverable endpoints...")
    api.add_resource(CalculateDeliverables, '/com/calculatedeliverables')
    api.add_resource(UpdateDeliverables, '/com/updatedeliverables')
    api.add_resource(GetCompletedDeliveries, '/com/getcompleteddeliverables')
    api.add_resource(GetOpenDeliverables, '/com/getopendeliverables')