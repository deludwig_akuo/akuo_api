from flask_restful import Resource
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import obligations
from database.db import Session


class GetObligations(Resource):

    def get(self):
        try:
            retVal = []
            dbData = Session.query(obligations).all()
            for item in dbData:
                retVal.append({'id': item.id,
                               'description': item.description,
                               'enactDate': item.enactDate,
                               'startMonth': item.startMonth,
                               'contract': item.contractName,
                               'recipients': item.recipients,
                               'notes': item.notes,
                               'enabled': item.enabled,
                               'catagory': item.catagory,
                               'delay': item.delay,
                               'responsible': item.responsible,
                               'frequency': item.frequency})

            Session.remove()
            return retVal

        except Exception as ex:
            print('Error in GetProjects')
