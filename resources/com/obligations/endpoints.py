from resources.com.obligations.getObligations import GetObligations


def add_endpoints(api):
    print("Adding obligation endpoints...")
    api.add_resource(GetObligations, '/com/getobligations')