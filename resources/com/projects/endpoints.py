from resources.com.projects.getProjects import GetProjects


def add_endpoints(api):
    print("Adding project endpoints...")
    api.add_resource(GetProjects, '/com/getprojects')