from flask_restful import Resource
from flask import jsonify
from flask import request
# from flask_jwt_extended import(jwt_required, get_jwt_claims)
from database.models import projects
from database.db import Session
import sys, os


class GetProjects(Resource):

    def get(self):
        try:
            retVal = []
            dbData = Session.query(projects).all()
            for item in dbData:
                # Now get the obligations for the project
                obligations = []
                for ob in item.obligations:
                    obligations.append({'id': ob.id,
                                        'description': ob.description,
                                        'enactDate': str(ob.enactDate),
                                        'startMonth': ob.startMonth,
                                        'contract': ob.contractName,
                                        'recipients': ob.recipients,
                                        'notes': ob.notes,
                                        'enabled': ob.enabled,
                                        'catagory': ob.catagory,
                                        'delay': ob.delay,
                                        'responsible': ob.responsible,
                                        'frequency': ob.frequency
                    })

                retVal.append({'id': item.id, 'name': item.name, 'obligations': obligations})

            Session.remove()
            return retVal

        except Exception as ex:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
