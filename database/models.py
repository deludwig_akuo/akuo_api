from sqlalchemy import Column, Integer, String, REAL, DATETIME, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class projects(Base):
    __tablename__ = 'projects'
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', String(100))
    obligations = relationship('obligations', backref='projects')


class obligations(Base):
    __tablename__ = 'obligations'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    description = Column('description', String(300))
    enactDate = Column('enactDate', DATETIME)
    startMonth = Column('startMonth', Integer)
    # interval = Column('interval', String(20))
    contractName = Column('contractName', String(300))
    recipients = Column('recipients', String(300))
    notes = Column('notes', String(300))
    enabled = Column('enabled', Boolean)
    catagory = Column('catagory', String(50))
    delay = Column('delay', Integer)
    frequency = Column('frequency', String(20))
    responsible = Column('responsible', String(80))
    # TODO: Add relationship to project
    projectId = Column('projectId', Integer, ForeignKey('projects.id'))
    deliverables = relationship('deliverables', backref='deliverables')
    # project = relationship('projects', back_populates='obligations')


class deliverables(Base):
    __tablename__ = 'deliverables'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    projectId = Column('projectId', Integer, ForeignKey('projects.id'))
    obligationId = Column('obligationId', Integer, ForeignKey('obligations.id'))
    dueDate = Column('dueDate', DATETIME)
    isComplete = Column('isComplete', Boolean)
    hoursToComplete = Column('hoursToComplete', Integer)
    notes = Column('notes', String(300))

class Users(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(128), unique=True, nullable=False)
    password = Column(String(128), nullable=False)
    email = Column(String(128))
    accessLevel = Column(Integer)
    companyID = Column(Integer)
    districtID = Column(Integer)
    phone = Column(String(20))
    accountResetKey = Column(String(50))


class OperatorLog(Base):
    __tablename__ = 'operatorLog'

    id = Column(Integer, primary_key=True)
    # userId = Column(Integer, ForeignKey('users.id'))
    timestamp = Column(DATETIME)
    projectId = Column(Integer, ForeignKey('projects.id'))
    tagId = Column(Integer, ForeignKey('olTags.id'))
    equipmentName = Column(String(100))
    type = Column(Integer)
    invoiceable = Column(Boolean)
    mal = Column(Boolean)
    closedBreaker = Column(Boolean)
    breakerName = Column(String(50))
    description = Column(String(500))
    notes = Column(String(500))
    tag = relationship('olTags')
    project = relationship('projects')
    # user = relationship('Users')


class olTags(Base):
    __tablename__ = 'olTags'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))

class RevokedTokens(Base):
    __tablename__ = 'revokedTokens'

    id = Column(Integer, primary_key=True)
    jti = Column(String(120))