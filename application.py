import os
from flask import Flask, render_template, request, jsonify
# import setproctitle
from flask_restful import Api
from flask_jwt_extended import (JWTManager)
from flask_cors import CORS
# from resources.com.projects.getProjects import GetProjects
# from resources.com.obligations.getObligations import GetObligations
from resources.root import Root
# from database.db import *

import resources.com.projects.endpoints
import resources.com.obligations.endpoints
import resources.com.deliverables.endpoints
import resources.sessions.endpoints
import resources.operatorLog.endpoints
import resources.data.endpoints
from resources.sessions.revokedTokenHelper import RevokedTokenHelper

application = Flask(__name__)

application.config['JWT_SECRETE_KEY'] = 'akuo'
application.secret_key = 'akuo'
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
application.config['SQLALCHEMY_DATABASE_URL'] = os.environ.get('DATABASE_URL', 'mysql+pymysql://admin:Maggie123!@database-1.cu4hydrcpave.us-east-2.rds.amazonaws.com/contractObligations')
application.config['JWT_BLACKLIST_ENABLED'] = True
application.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
api = Api(application)

CORS(application, support_credentials=True)

# ******* Setup 404 handler *********
@application.errorhandler(404)
def not_found(e):
    return render_template('404.html')

# ******** Appliation Events ************
@application.route("/")
def index():
    print(application.root_path)
    return render_template('index.html')

# Setup the JWT token
jwt = JWTManager(application)

@jwt.token_in_blocklist_loader
def check_if_token_in_blacklist(jwt_header, jwt_playload):
    jti = jwt_playload['jti']
    helper = RevokedTokenHelper()
    return helper.is_jti_blacklisted(jti)
        # return jsonify({'Message': 'Please login first', 'status': 0}), 401

@jwt.revoked_token_loader
def revoked_token_loader(revokedToken, other):
    # jwtkn = revokedToken['jti']
    return jsonify({'Message': 'Please login first', 'status': 0}), 401


# @jwt.user_claim_loader
# def add_claims_to_access_token(identity):
#     # Get data
#     accessLevel = 1
#     userId = 'userId'
#
#     return {
#         'hello': identity,
#         'accessLevel': accessLevel,
#         'userId': userId
#     }

api.add_resource(Root, '/')

resources.com.projects.endpoints.add_endpoints(api)
resources.com.obligations.endpoints.add_endpoints(api)
resources.com.deliverables.endpoints.add_endpoints(api)
resources.sessions.endpoints.add_endpoints(api)
resources.operatorLog.endpoints.add_endpoints(api)
resources.data.endpoints.add_endpoints(api)

# db.init_app(application)

if __name__ == '__main__':
    application.run(host='0.0.0.0', debug=True)