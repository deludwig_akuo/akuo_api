"""modify operatorLog

Revision ID: a9d99a48867e
Revises: 99926f3ec15f
Create Date: 2022-01-28 22:57:50.944440

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a9d99a48867e'
down_revision = '99926f3ec15f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
